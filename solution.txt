1. List the books Authored by Marjorie Green.
BU1032 - The Busy Executives Database Guide
BU2075 - You Can Combat Computer Stress!

2. List the books Authored by Michael O'Leary.
BU1111 - Cooking with Computers
TC7777 - (No longer in the database)

3. Write the author/s of the "The Busy Executives Database Guide".
213-46-8915 - Marjorie Green
409-56-7008 - Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".
1389 - Algodata Infosystems

5. List the books published by Algodata Infosystems.
BU1032 - The Busy Executive's Database Guide
BU1111 - Cooking with Computers
BU7832 - Straight Talk About Computers
PC1035 - But Is It User Friendly?
PC8888 - Secrets of Silicon Valley
PC9999 - Net Etiquette